package sales_item_solver.beans;

import java.util.HashMap;

public class Parameter extends HashMap<Object,Object> {

	private static final long serialVersionUID = 1L;

	public Parameter() {
		super();
	}
	public Parameter(String aLeft, String aRight){
		this.put(aLeft, aRight);
	}
	
	public Parameter add(String aLeft, String aRight) {
		this.put(aLeft, aRight);
		return this;
	}
	
	public Object pop() {
		Object wResult;
		try {
			wResult = this.get(this.keySet().iterator().next());
		}catch(Throwable t){
			wResult = new Object();
		}
		return wResult;
	}
	
}
