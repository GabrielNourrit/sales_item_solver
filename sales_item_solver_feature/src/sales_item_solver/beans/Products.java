package sales_item_solver.beans;

import java.util.ArrayList;

public class Products extends ArrayList<Product> implements IProducts {
	private static final long serialVersionUID = -3768500075701790321L;

	@Override
	public String toHtml() {
		final String HEAD = "<!doctype html>\n" + 
				"<head>\n" + 
				"  <title>results</title>\n" + 
				"  <meta charset=\"UTF-8\">\n" + 
				"  \n" + 
				" <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" + 
				"\n" + 
				"</head>\n" + 
				"\n" + 
				"<body>\n" + 
				"  <table class=\"table\">\n" + 
				"  <thead class=\"thead-dark\">\n" + 
				"    <tr>\n" + 
				"      <th scope=\"col\">Price</th>\n" + 
				"      <th scope=\"col\">Image</th>\n" + 
				"      <th scope=\"col\">Title</th>\n" + 
				"      <th scope=\"col\">to</th>\n" + 
				"      <th scope=\"col\">from</th>\n" + 
				"      <th scope=\"col\">repackaged</th>\n" + 
				"      <th scope=\"col\">link</th>\n" + 
				"    </tr>\n" + 
				"  </thead>\n" + 
				"  <tbody>";
		final String FOOT = "</tbody>\n" + 
				"  <tfoot class=\"thead-dark\">\n" + 
				"    <tr>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"      <th scope=\"col\"></th>\n" + 
				"    </tr>\n" + 
				"  </tfoot>\n" + 
				"</table>\n" + 
				"\n" + 
				"\n" + 
				"  \n" + 
				"\n" + 
				"<!-- Latest compiled and minified JavaScript -->\n" + 
				"<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n" + 
				"<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n" + 
				"<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script><script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n" + 
				"</body>\n" + 
				"</html>";

		String wHtml = HEAD;
		for(Product wProduct : this) {
			wHtml += wProduct.toHtml();
		}
		wHtml += FOOT;
		return wHtml;
	}
}
