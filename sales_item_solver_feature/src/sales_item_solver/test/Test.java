package sales_item_solver.test;

import java.io.PrintWriter;

import sales_item_solver.beans.IProducts;
import sales_item_solver.utils.Agent;
import sales_item_solver.webdirectories.CrossShopper;

public class Test {
	private static Agent pAgent = new Agent();

	public static void main(String[] args) throws Exception {
		CrossShopper wCrossShopper = new CrossShopper(pAgent);


		IProducts wProducts = wCrossShopper.search("");
		
		String wHtml = wProducts.toHtml();


		PrintWriter writer = new PrintWriter("/home/nourritg/Bureau/results.html", "UTF-8");
		writer.println(wHtml);
		writer.close();
	}

}
