package sales_item_solver.webdirectories;

import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONObject;

import sales_item_solver.beans.Parameter;
import sales_item_solver.beans.Product;
import sales_item_solver.beans.Products;
import sales_item_solver.utils.Agent;
import sales_item_solver.utils.BuilderGetRequest;

public class EGlobalCentral implements IEGlobalCentralConstants{
	Agent pAgent;

	public EGlobalCentral(Agent aAgent) {
		pAgent = aAgent;
	}
	
	/**
	 * apply the good pattern on aSearch in order to use the result in a post parameter
	 * @param aSearch
	 * @return aSearchFormatted
	 */
	private String format(String aSearch) {
		return  aSearch.replaceAll("\\s", "+");
	}
	
	@Override
	public Products search(String aSearch) {
		//configuration request
		String wSearchFormatted = format(aSearch);
		BuilderGetRequest wBuilderRequest=  new BuilderGetRequest();
		String wParameters = wBuilderRequest
				.addParameters("api_key",URLEncoder.encode(API_KEY))
				.addParameters("q",URLEncoder.encode(wSearchFormatted))
				.addParameters("maxResults",URLEncoder.encode(MAX_RESULTS))
				.addParameters("items",URLEncoder.encode(ITEMS_STATE))
				.addParameters("pages",URLEncoder.encode(PAGES_STATE))
				.addParameters("facets",URLEncoder.encode(FACETS_STATE))
				.addParameters("categories",URLEncoder.encode(CATEGORIES_STATES))
				.addParameters("suggestions",URLEncoder.encode(SUGGESTIONS_STATE))
				.addParameters("pagesMaxResults", URLEncoder.encode(PAGES_MAX_RESULTS))
				.addParameters("categoriesMaxResults", URLEncoder.encode(CATEGORIES_MAX_RESULTS))
				.addParameters("suggestionsMaxResults", URLEncoder.encode(SUGGESTION_MAX_RESULTS))
				.addParameters("output", URLEncoder.encode(JSON))
				.build();
		
		
		Products wProducts = new Products();
		Parameter wConstraints;
		try {
			//request
			JSONArray wJsonProducts = new JSONObject(pAgent.get(BASE + 
					RESTRICTS_BY +
					INDEXS +
					wParameters
					,new Parameter())).getJSONArray("items");
			
			//create products
			JSONObject wJsonProduct = null; 
			for(int i = 0 ; i < wJsonProducts.length() ; i++) {
				wJsonProduct = (JSONObject) wJsonProducts.get(i);
				wConstraints = new Parameter("image",wJsonProduct.getString("image_link"));
				wProducts.add(new Product(
						wJsonProduct.getString("title"),
						wConstraints,
						wJsonProduct.getDouble("price"),
						"France",
						"Monde",
						false, 
						wJsonProduct.getString("link")
						));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return wProducts;
	}
	

}
