package sales_item_solver.webdirectories;

import java.net.URLEncoder;

public interface IEGlobalCentralConstants extends ISite{
	final String BASE = "https://www.searchanise.com/getwidgets?";
	final String API_KEY = "6u5o3O0t5H";
	final String RESTRICTS_BY = "restrictBy[status]=A&"+
			"restrictBy[empty_categories]=N&"+
			"restrictBy[usergroup_ids]="+URLEncoder.encode("0|1")+"&"+
			"restrictBy[category_usergroup_ids]="+URLEncoder.encode("0|1")+"&";
	final String INDEXS = "startIndex=0&"+
			"pageStartIndex=0&"+
			"categoryStartIndex=0&";
	final String ITEMS_STATE = "true";
	final String PAGES_STATE = "false";
	final String FACETS_STATE = "false";
	final String CATEGORIES_STATES = "false";
	final String SUGGESTIONS_STATE = "false";
	final String PAGES_MAX_RESULTS = "0";
	final String CATEGORIES_MAX_RESULTS = "0";
	final String SUGGESTION_MAX_RESULTS = "0";
	final String MAX_RESULTS = "6";
	final String JSON = "jsonp";
}
