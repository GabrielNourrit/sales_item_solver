package sales_item_solver.utils;



public class BuilderGetRequest {
	private String pResponse = "";
	
	
	
	public BuilderGetRequest addParameters(String aLeftHand, String aRightHand) {
			
			if (! pResponse.equals("")) pResponse += "&"; 
			pResponse += aLeftHand+ "=" + aRightHand;
		
		return this;
	}
	
	public String build() {
		return this.pResponse;
	}
}
