package sales_item_solver.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;

import sales_item_solver.beans.Parameter;



public class Agent implements IAgent{
	public class AgentException extends Exception{
		private static final long serialVersionUID = 1L;

		public AgentException(int aErrorState) {
			super("BAD REQUEST ERROR "+aErrorState);
		}
	} 

	@Override
	public String get(String aUrl, Parameter aHeaders) {
StringBuffer wResponse = new StringBuffer();
		
		try {

			CloseableHttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(aUrl);

			// add request header
			for(Object key : aHeaders.keySet())
				request.addHeader(key.toString(), aHeaders.get(key));
			CloseableHttpResponse response = client.execute(request);

			int aCode = response.getCode();
			
			if(aCode > 399) throw new AgentException(aCode);

			BufferedReader rd = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				wResponse.append(line);
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
		}
		return wResponse.toString();
	}
	
	

}
