package sales_item_solver.utils;


import sales_item_solver.beans.Parameter;

public interface IAgent {
	String get(String aUrl, Parameter aHeaders);

}
