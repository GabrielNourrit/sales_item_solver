package gab.client.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import sales_item_solver.beans.IProducts;
import sales_item_solver.utils.Agent;
import sales_item_solver.webdirectories.CrossShopper;


@RequestMapping("/")
@Controller
public class RootController {
	 
	@GetMapping
    public String root(Model aModel) {
		
		Agent wAgent = new Agent();
		
		CrossShopper wCrossShopper = new CrossShopper(wAgent);

		IProducts wProducts = wCrossShopper.search("");
		
		aModel.addAttribute("aProducts", wProducts);
		
        return "produits";
    }
    
}